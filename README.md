# reforumapp-django

## The ReForum system
This repository is an implementation of the ReForum Systems 3 project in Django.

## Description
The system will enable users to communicate and agree more easily about large undertakings such as renovation or cleanup of public spaces, while keeping track of materials that were needed for a given project and those which have been acquired already.

## Authors and acknowledgment
Seminar and implementation by Andrej Milisavljević, 89201188, Bioinformatics EN

### Tutorials referenced:
[1] https://code.visualstudio.com/docs/python/tutorial-django (basic setup)
[2.] https://realpython.com/customize-django-admin-python/ (basic setup, customization)
[3.] https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Tutorial_local_library_website (used solely on dev-mdn branch, not merged into main - for learning and practice)
[4.] https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_sidenav_fixed (inspiration for styling the sidebar in CSS)


