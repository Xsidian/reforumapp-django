from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('projects/', views.ProjectListView.as_view(), name='projects'),
    path('project/<int:pk>', views.project_detail_view, name='project-detail'),
    path('project/<int:pk>/additem', views.project_detail_add_item, name='project-detail-add-item'),
    path('project/<int:pk>/addcomment', views.project_detail_add_comment, name='project-detail-add-comment'),
    path('project/<int:pk>/sendmessage', views.project_detail_send_message, name='project-detail-send-message'),
    path('project/<int:pk>/vote', views.project_detail_vote, name='project-detail-vote'),

    path('project/<int:pk>/delete-checklist-items/', views.delete_checklist_items, name='project-delete-checklist-items'),

    path('projects/<int:pk>/leave/', views.leave_project, name='project-leave'),

    path('sendinvite/', views.invite_user, name='invite-user'),


    path('myprojects/', views.MyProjectsListView.as_view(), name='my-projects'),

    path('project/create/', views.ProjectCreate.as_view(), name='project-create'),
    path('project/<int:pk>/update/', views.ProjectUpdate.as_view(), name='project-update'),
    path('project/<int:pk>/delete/', views.ProjectDelete.as_view(), name='project-delete'),

    path('invites/', views.project_invites, name='project-invites'),
    path('invites/<int:invite_pk>/', views.handle_invite, name='accept-invite'),
]


