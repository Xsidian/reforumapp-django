from django.contrib.auth.mixins import UserPassesTestMixin


class UserIsProjectOwnerMixin(UserPassesTestMixin):
    def test_func(self):
        project = self.get_object()
        return self.request.user == project.author_user