from django.contrib import admin

# Register your models here.
from .models import Project, ChecklistItem, Vote, Comment, Message, Invite #, GroupChat

admin.site.register(Project)
admin.site.register(ChecklistItem)
admin.site.register(Vote)
admin.site.register(Comment)
#admin.site.register(GroupChat)
admin.site.register(Message)
admin.site.register(Invite)
