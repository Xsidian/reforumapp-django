import datetime

from reforum import settings
from .models import ChecklistItem, Invite, Project
from django import forms
from datetime import datetime
#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()

class ItemForm(forms.Form):
    description = forms.CharField(max_length=1000, widget=forms.Textarea(attrs={'cols': 59, 'rows': 1}), label='')
    
    
class CustomCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option = super().create_option(name, value, label, selected, index, subindex, attrs)

        # get CheckItem instance for the current option
        check_item = self.choices.queryset[index]
        # append form submit function to send post each time checkbox is ticked 
        # (a single save button would also work here)
        option["attrs"]["onchange"] = "this.form.submit()"
        # add "checked" html attribute if is_checked is True
        if check_item.is_checked:
            option['attrs']['checked'] = 'checked'
        return option
    
class CheckForm(forms.Form):
    checks = forms.ModelMultipleChoiceField(
        queryset= ChecklistItem.objects.all(),
        widget = CustomCheckboxSelectMultiple,
        )
    def __init__(self, project, *args, **kwargs):
        super(CheckForm, self).__init__(*args, **kwargs)
        if project:
            query = ChecklistItem.objects.filter(project = project)
            self.fields['checks'].queryset = query
            self.fields['checks'].required = False
        else:
            ValueError("project not provided")



class DateSelectorWidget(forms.MultiWidget):
    def __init__(self, attrs=None, date_format='%Y-%m-%d', time_format='%H:%M:%S'):

        widgets = [
            forms.DateInput(attrs={'type': 'date', 'format': date_format}),
            forms.TimeInput(attrs={'type': 'time', 'format': time_format}),
        ]
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.date(), value.time()]
        else:
            # default to the current date and time with seconds set to 00
            current = datetime.now().replace(minute=0, second=0)
            return [current.date(), current.time()]

    def value_from_datadict(self, data, files, name):
        date_value = self.widgets[0].value_from_datadict(data, files, name + '_0')
        time_value = self.widgets[1].value_from_datadict(data, files, name + '_1')
        return datetime.strptime(date_value+" "+time_value, "%Y-%m-%d %H:%M:%S")



class ProjectModelForm(forms.ModelForm):
    held_date = forms.DateTimeField(widget=DateSelectorWidget)
    def __init__(self, *args, user=None, **kwargs):
        super(ProjectModelForm, self).__init__(*args, **kwargs)
        self.fields['lat'].required = True
        self.fields['long'].required = True
        self.fields['lat'].error_messages = {'required': 'Please enter the latitude using the map'}
        self.fields['long'].error_messages = {'required': 'Please enter the longitude using the map'}

    class Meta:
        model=Project
        fields = ["name","description","held_date","lat","long"]
        widgets = {
            'lat': forms.TextInput(attrs={'id': 'map-lat'}),
            'long': forms.TextInput(attrs={'id': 'map-long'}),
        }

class InviteForm(forms.ModelForm):
    invited_username = forms.CharField(label='Send invite to:', max_length=255)

    class Meta:
        model = Invite
        fields = ['invited_username', 'project']

    def __init__(self, *args, user=None, **kwargs):
        super(InviteForm, self).__init__(*args, **kwargs)
        self.fields['project'].queryset = Project.objects.filter(member_users=user)

    def clean_invited_username(self):
        invited_username = self.cleaned_data['invited_username']
        invited_user = User.objects.filter(username=invited_username).first()

        if not invited_user:
            raise forms.ValidationError("User not found. Please enter a valid username.")

        return invited_user

    def save(self, commit=True):
        invite = super().save(commit=False)
        invite.invited_user = self.cleaned_data['invited_username']

        if commit:
            invite.save()

        return invite


class CommentForm(forms.Form):
    text = forms.CharField(max_length=1000, widget=forms.Textarea(attrs={'cols': 70, 'rows': 3}), label='')

class MessageForm(forms.Form):
    text = forms.CharField(max_length=1000, widget=forms.Textarea(attrs={'cols': 70, 'rows': 1}), label='')


from django.contrib.auth.forms import UserCreationForm

class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].required = True
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('first_name', 'last_name', 'email')

class VoteForm(forms.Form):
    VOTE_CHOICES = (
        ('upvote', 'Upvote'),
        ('downvote', 'Downvote'),
        ('reset', 'Reset vote'),
    )
    
    vote_action = forms.ChoiceField(
        choices=VOTE_CHOICES,
        widget=forms.RadioSelect,
        required=True,
    )


class CheckDeleteForm(forms.Form):
    checks = forms.ModelMultipleChoiceField(
        queryset=ChecklistItem.objects.all(),
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, project, *args, **kwargs):
        super(CheckDeleteForm, self).__init__(*args, **kwargs)
        if project:
            query = ChecklistItem.objects.filter(project=project)
            self.fields['checks'].queryset = query
            self.fields['checks'].required = False
        else:
            raise ValueError("project not provided")
