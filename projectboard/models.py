from django.db import models
from django.urls import reverse
from django.conf import settings
from datetime import datetime

class Project(models.Model):

    author_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name="author_user")
 
    name = models.CharField(max_length=100, help_text='')
    description = models.TextField(max_length=1000)
    post_date = models.DateTimeField(auto_now_add=True, help_text = "The time the project was created.")
    held_date = models.DateTimeField(help_text = "")

    lat = models.DecimalField(max_digits=22, decimal_places=16, blank=False, null=False)
    long = models.DecimalField(max_digits=22, decimal_places=16, blank=False, null=False)

    # project has many members, members may be in multiple projects
    # no need for "through" table as we're not saving any additional data (such as when the user joined the chat)
    member_users = models.ManyToManyField(settings.AUTH_USER_MODEL, help_text="Users taking part in the project", related_name="member_users")
    
    class Meta:
        ordering = ['-post_date']

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('project-detail', args=[str(self.id)])

    def points(self):
        pos_votes =  self.vote_set.filter(project = self, is_positive = True).count()
        neg_votes = self.vote_set.filter(project = self, is_positive = False).count()
        return pos_votes - neg_votes
    
    # def save_model(self, request, instance, form, change):
    #     user = request.user 
    #     instance = form.save(commit=False)
    #     if not change or not instance.author_user:
    #         instance.author_user = user
    #     instance.save()
    #     form.save_m2m()
    #     return instance


    @property
    def is_active(self):
        return bool(self.held_date and datetime.today().timestamp() <= self.held_date.timestamp())
    




class ChecklistItem(models.Model):

    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True)
    description = models.CharField(max_length=100, help_text='Enter the checklist item description.') 
    is_checked = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.description}'

class Vote(models.Model):

    voter = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True)
    is_positive = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.voter} {"up" if self.is_positive else "down"}voted \"{self.project.name}\"'


class Comment(models.Model):

    commenter = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True)
    text = models.TextField(max_length=1000)
    post_date = models.DateTimeField(auto_now_add=True, help_text = "The time the comment was created.")

    def __str__(self):
        length = 90
        return f'{self.commenter} commented \"{self.text[0:length]}{"" if len(self.text)<length else "..."}\" on \"{self.project.name}\"'

    def get_absolute_url(self):
        return reverse("commentdetail", kwargs={"pk": self.pk})


class Invite(models.Model):
    inviting_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name="inviting_user")
    invited_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name="invited_user")
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True)
    accepted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.inviting_user} invited \"{self.invited_user}\" to \"{self.project.name}\"'

    def accept_invite(self):
        if not self.accepted:
            self.accepted = True
            self.project.member_users.add(self.invited_user)
            self.save()


class Message(models.Model):

    poster = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True)
    post_date = models.DateTimeField(auto_now_add=True, help_text = "The time the comment was created.")
    text = models.TextField(max_length=1000)

    class Meta:
        ordering = ['post_date']

    def __str__(self):
        length = 90
        return f'{self.poster} wrote \"{self.text[0:length]}{"" if len(self.text)<length else "..."}\" on \"{self.project.name}\"'
