from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.models import User
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CheckDeleteForm, CheckForm, InviteForm, ItemForm, ProjectModelForm, CommentForm, MessageForm, VoteForm
from .models import Project, Invite, Comment, ChecklistItem, Message, Vote
from .mixins import UserIsProjectOwnerMixin
from django.db.models import Q
from projectboard.forms import CustomUserCreationForm
from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth import login

def index(request):
    
    n_projects = Project.objects.all().count()
    n_users = User.objects.all().count()
    n_visits = request.session.get('n_visits', 0)
    request.session['n_visits'] = n_visits + 1

    # sending data as context to the html template
    context = {
        'n_projects' : n_projects,
        'n_users' : n_users,
        'n_visits' : n_visits
    }

    return render(request, 'index.html', context=context)



class ProjectListView(generic.ListView):
    model = Project
    paginate_by=10
    template_name = 'projectboard/project_list.html'
    def get_queryset(self):
        query = self.request.GET.get('q')
        print(f"Search Query: {query}")
        if query:
            return Project.objects.filter(
                Q(name__icontains=query) |
                Q(description__icontains=query)
            )
        else:
            return Project.objects.all()

# changed to function based view as I found it much easier to work with compared to overriding class functions
def project_detail_view(request, pk):
    context = {}
    #votes = Vote.objects.all()
    project = Project.objects.get(id=pk)

    # pos_votes =  votes.filter(project = project, is_positive = True).count()
    # neg_votes = votes.filter(project = project, is_positive = False).count()
    # points = pos_votes - neg_votes

    item_form = ItemForm()
    list_form = CheckForm(project = project)
    comment_form = CommentForm()
    message_form = MessageForm()
    vote_form = VoteForm()
    
    # context["points"] = points
    context["project"] = project

    if request.method == "POST":
        list_form = CheckForm(project, request.POST)
        #print("clicked")
        if list_form.is_valid():
            #print("valid")
            #print(str(list_form.data))
            data = list_form.cleaned_data
            
            for checkitem in ChecklistItem.objects.filter(project = project):
                if checkitem in data["checks"]:
                    checkitem.is_checked=True
                else:
                    checkitem.is_checked=False
                checkitem.save()
            return HttpResponseRedirect(f'{project.id}#checklist') # focus back onto checklist part of page
        #else:
            #print(request.POST)    
            #print(list_form.errors)
    context["forms"] = {"item": item_form, "list": list_form, "comment": comment_form, "message": message_form, "vote" : vote_form}
    return render(request, 'projectboard/project_detail.html', context=context)


def delete_checklist_items(request, pk):
    project = get_object_or_404(Project, id=pk)
    form = CheckDeleteForm(project=project)

    if request.method == "POST":
        form = CheckDeleteForm(project, request.POST)
        if form.is_valid():
            data = form.cleaned_data
            for checkitem in ChecklistItem.objects.filter(project=project):
                if checkitem in data["checks"]:
                    checkitem.delete()

            redirect_url = f"/projectboard/project/{pk}#checklist"
            return redirect(redirect_url)

    context = {"project": project, "form": form}
    return render(request, 'projectboard/delete_checklist_items.html', context=context)

def project_detail_add_item(request, pk):
    project = get_object_or_404(Project, id=pk)

    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # process
            item = ChecklistItem(
                project = project,
                description = data["description"],
                is_checked = False,
                )
            item.save()
    redirect_url = f"/projectboard/project/{pk}#checklist"
    return redirect(redirect_url)

def project_detail_add_comment(request, pk):
    project = get_object_or_404(Project, id=pk)

    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # process
            item = Comment(
                project = project,
                text = data["text"],
                commenter = request.user
                )
            item.save()
    redirect_url = f"/projectboard/project/{pk}#commentlist"
    return redirect(redirect_url)



def project_detail_send_message(request, pk):
    project = get_object_or_404(Project, id=pk)

    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # process
            item = Message(
                project = project,
                text = data["text"],
                poster = request.user
                )
            item.save()

    redirect_url = f"/projectboard/project/{pk}#chat"
    return redirect(redirect_url)


def project_detail_vote(request, pk):
    project = Project.objects.get(pk=pk)
    user = request.user

    try:
        vote = Vote.objects.get(voter=user, project=project)
    except Vote.DoesNotExist:
        vote = None

    if request.method == 'POST':
        form = VoteForm(request.POST)

        if form.is_valid():
            vote_action = form.cleaned_data['vote_action']

            if vote_action == 'upvote':
                if not vote:
                    Vote.objects.create(voter=user, project=project, is_positive=True)
                else:
                    vote.is_positive = True
                    vote.save()

            elif vote_action == 'downvote':
                if not vote:
                    Vote.objects.create(voter=user, project=project, is_positive=False)
                else:
                    vote.is_positive = False
                    vote.save()

            elif vote_action == 'reset':
                if vote:
                    vote.delete()

    return redirect("project-detail", project.id)


def invite_user(request):
    if request.method == 'POST':
        form = InviteForm(request.POST, user=request.user.id)
        if form.is_valid():
            invite = form.save(commit=False)
            invite.inviting_user = request.user
            invite.save()
            return redirect("my-projects")
    else:
        form = InviteForm(user=request.user.id)

    return render(request, 'projectboard/invite_form.html', {'form': form})


def project_invites(request):
    user_invites = Invite.objects.filter(invited_user=request.user.id, accepted=False)
    return render(request, 'projectboard/project_invites.html', {'user_invites': user_invites})


def handle_invite(request, invite_pk):
    invite = get_object_or_404(Invite, id=invite_pk, invited_user=request.user.id, accepted=False)

    if request.method == 'POST':
        action = request.POST.get('action')

        if action == 'accept':
            invite.accept_invite()
        elif action == 'delete':
            invite.delete()

    return redirect('project-invites')


def leave_project(request, pk):
    project = get_object_or_404(Project, id=pk)
    #print("hello!...")
    if request.method == 'POST':
        #print("post...")
        if request.user == project.author_user:
            #print("user is author")
            # if the author is trying to leave, do nothing
            return redirect('project-detail', project.id)
        # else remove him from members
        #print("removing user...")
        project.member_users.remove(request.user)

    return redirect('project-detail', project.id)


def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('index')
    else:
        form = CustomUserCreationForm()
    
    return render(request, 'registration/register.html', {'form': form})

# class-based project  list view and project creation views

class MyProjectsListView(LoginRequiredMixin,generic.ListView):
    model = Project
    template_name = 'projectboard/my_projects_list.html'
    paginate_by = 10

    def get_queryset(self):
        return (
            Project.objects.filter(member_users=self.request.user)
            .order_by('-held_date')
        )


class ProjectCreate(LoginRequiredMixin, CreateView):

    model = Project
    # widgets only work and can be defined with an explicit form, not overridden here
    form_class = ProjectModelForm
    def form_valid(self, form):
        # set author_user field to current user
        form.instance.author_user = self.request.user
        # save form to create Project instance and get id
        response = super().form_valid(form)

        # clear existing member_users and add the current user
        self.object.member_users.clear()
        self.object.member_users.add(self.request.user)
        return response
        
class ProjectUpdate(LoginRequiredMixin, UserIsProjectOwnerMixin, UpdateView):
    model = Project
    form_class = ProjectModelForm
    #fields = ["name","description","held_date", "lat", "long"] # w.o 'member_users'


class ProjectDelete(LoginRequiredMixin, UserIsProjectOwnerMixin, DeleteView):
    model = Project
    success_url = reverse_lazy('projects')

    def form_valid(self, form):
        try:
            self.object.delete()
            return HttpResponseRedirect(self.success_url)
        except Exception as e:
            return HttpResponseRedirect(
                reverse("project-delete", kwargs={"pk": self.object.pk})
            )



